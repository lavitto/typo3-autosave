<?php
/**
 * This file is part of the "autosave" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

return [
    'autosave_get_button_status' => [
        'path' => '/autosave/get_button_status',
        'target' => \Lavitto\Autosave\Controller\AjaxController::class . '::getButtonStatusAction'
    ],
    'autosave_switch_status' => [
        'path' => '/autosave/switch_status',
        'target' => \Lavitto\Autosave\Controller\AjaxController::class . '::switchAutosaveStatusAction'
    ]
];
