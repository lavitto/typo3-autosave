<?php
/**
 * This file is part of the "autosave" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

namespace Lavitto\Autosave\Utility;

use TYPO3\CMS\Core\Authentication\BackendUserAuthentication;
use TYPO3\CMS\Core\SingletonInterface;

/**
 * Class AutosaveUtility
 *
 * @package Lavitto\Autosave\Utility
 */
class AutosaveUtility implements SingletonInterface
{

    /**
     * Switches the autosave status
     */
    public function switchAutosave(): bool
    {
        if ($this->isAutosaveEnabled() === true) {
            $this->disableAutosave();
            $newStatus = false;
        } else {
            $this->enableAutosave();
            $newStatus = true;
        }
        return $newStatus;
    }

    /**
     * Checks if autosave is enabled
     *
     * @return bool
     */
    public function isAutosaveEnabled(): bool
    {
        $moduleData = $this->getModuleData();
        return $moduleData['status'] === true;
    }

    /**
     * Enables the Autosave function in BE_USER settings
     */
    public function enableAutosave(): void
    {
        $moduleData = $this->getModuleData();
        $moduleData['status'] = true;
        $this->setModuleData($moduleData);
    }

    /**
     * Disables the Autosave function in BE_USER settings
     */
    public function disableAutosave(): void
    {
        $moduleData = $this->getModuleData();
        $moduleData['status'] = false;
        $this->setModuleData($moduleData);
    }

    /**
     * Gets the module data of the BE_USER session
     *
     * @return array
     */
    protected function getModuleData(): array
    {
        return $this->getBackendUser()->getModuleData('autosave') ?? [];
    }

    /**
     * Sets the module data of the BE_USER session
     *
     * @param array $moduleData
     */
    protected function setModuleData(array $moduleData): void
    {
        $this->getBackendUser()->pushModuleData('autosave', $moduleData);
    }

    /**
     * Returns the backend user
     *
     * @return BackendUserAuthentication
     */
    protected function getBackendUser(): BackendUserAuthentication
    {
        return $GLOBALS['BE_USER'];
    }
}
