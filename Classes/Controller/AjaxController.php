<?php
/**
 * This file is part of the "autosave" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

namespace Lavitto\Autosave\Controller;

use Lavitto\Autosave\Utility\AutosaveUtility;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use TYPO3\CMS\Backend\Controller\AbstractFormEngineAjaxController;
use TYPO3\CMS\Core\Http\JsonResponse;
use TYPO3\CMS\Core\Imaging\Icon;
use TYPO3\CMS\Core\Imaging\IconFactory;
use TYPO3\CMS\Core\Localization\LanguageService;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;

/**
 * Class AjaxController
 *
 * @package Lavitto\Autosave\Controller
 */
class AjaxController extends AbstractFormEngineAjaxController
{

    /**
     * Object Manager
     *
     * @var ObjectManager
     */
    protected $objectManager;

    /**
     * Autosave Utility
     *
     * @var AutosaveUtility
     */
    protected $autosaveUtility;

    /**
     * Icon factory
     *
     * @var IconFactory
     */
    protected $iconFactory;

    /**
     * AjaxController constructor.
     */
    public function __construct()
    {
        $this->objectManager = GeneralUtility::makeInstance(ObjectManager::class);
        $this->iconFactory = $this->objectManager->get(IconFactory::class);
        $this->autosaveUtility = $this->objectManager->get(AutosaveUtility::class);
    }

    /**
     * Checks the autosave-status
     *
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     */
    public function getButtonStatusAction(ServerRequestInterface $request): ResponseInterface
    {
        $status = $this->autosaveUtility->isAutosaveEnabled();
        return $this->getResponse($status);
    }

    /**
     * Switchs the current autosave status
     *
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     */
    public function switchAutosaveStatusAction(ServerRequestInterface $request): ResponseInterface
    {
        $status = $this->autosaveUtility->switchAutosave();
        return $this->getResponse($status);
    }

    /**
     * Creates and returns the JsonResponse
     *
     * @param bool $status
     * @return ResponseInterface
     */
    protected function getResponse(bool $status): ResponseInterface
    {
        return new JsonResponse([
            'status' => $status,
            'buttonContent' => $this->getButtonContent($status),
            'buttonTitle' => $this->getButtonTitle($status)
        ]);
    }

    /**
     * Returns the button content by a given status
     *
     * @param bool $status
     * @return string
     */
    protected function getButtonContent(bool $status): string
    {
        $statusString = $status === true ? 'enabled' : 'disabled';
        $icon = $this->iconFactory->getIcon('ext-autosave-autosave-' . $statusString, Icon::SIZE_SMALL);
        $label = $this->getLanguageService()->sL('LLL:EXT:autosave/Resources/Private/Language/locallang.xlf:autosave.' . $statusString);
        return $icon . ' ' . $label;
    }

    /**
     * Returns the button title by a given status
     *
     * @param bool $status
     * @return string
     */
    protected function getButtonTitle(bool $status): string
    {
        $actionString = $status === true ? 'disable' : 'enable';
        return $this->getLanguageService()->sL('LLL:EXT:autosave/Resources/Private/Language/locallang.xlf:autosave.' . $actionString);
    }

    /**
     * Returns the language service
     *
     * @return LanguageService
     */
    protected function getLanguageService(): LanguageService
    {
        return $GLOBALS['LANG'];
    }
}
