<?php
/**
 * This file is part of the "autosave" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

namespace Lavitto\Autosave\Hooks;

use TYPO3\CMS\Backend\Template\Components\ButtonBar;
use TYPO3\CMS\Backend\Template\Components\Buttons\InputButton;
use TYPO3\CMS\Core\Imaging\Icon;
use TYPO3\CMS\Core\Imaging\IconFactory;
use TYPO3\CMS\Core\Localization\LanguageService;
use TYPO3\CMS\Core\Page\PageRenderer;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class AutosaveHook
 *
 * @package Lavitto\Autosave\Hooks
 */
class AutosaveHook
{

    /**
     * Default save interval, if there is no configuration (default 300s / 5min)
     */
    protected const DEFAULT_INTERVAL = 300;

    /**
     * Icon Factory
     *
     * @var IconFactory
     */
    protected $iconFactory;

    /**
     * Page Renderer
     *
     * @var PageRenderer
     */
    protected $pageRenderer;

    /**
     * AutosaveHook constructor.
     */
    public function __construct()
    {
        $this->iconFactory = GeneralUtility::makeInstance(IconFactory::class);
        $this->pageRenderer = GeneralUtility::makeInstance(PageRenderer::class);
    }

    /**
     * Adds the autosave button to the button bar, if there is a "save"-button available
     *
     * @param array $params
     * @param ButtonBar $buttonBar
     * @return array
     */
    public function addAutosaveButton(array $params, ButtonBar $buttonBar): array
    {
        $buttons = $params['buttons'];
        $saveButton = $params['buttons'][ButtonBar::BUTTON_POSITION_LEFT][2][0];
        if ($saveButton instanceof InputButton) {
            $icon = $this->iconFactory->getIcon('ext-autosave-autosave', Icon::SIZE_SMALL);
            $title = $this->getLanguageService()->sL('LLL:EXT:autosave/Resources/Private/Language/locallang.xlf:button_loading');
            $autoSaveButton = $buttonBar->makeInputButton()
                ->setClasses('btn-autosave')
                ->setDataAttributes(['toggle' => 'tooltip', 'placement' => 'left'])
                ->setDisabled(true)
                ->setTitle($title)
                ->setIcon($icon)
                ->setShowLabelText(true);
            $buttons[ButtonBar::BUTTON_POSITION_RIGHT][][] = $autoSaveButton;
            $this->pageRenderer->loadRequireJsModule('TYPO3/CMS/Autosave/Autosave');
            $this->addJavaScriptLocalization($icon . ' ' . $title);
            $this->addJavaScriptSettings();
        }
        return $buttons;
    }

    /**
     * Adds the localizations for javascript to the page
     *
     * @param string $loadingButtonContent
     */
    protected function addJavaScriptLocalization(string $loadingButtonContent): void
    {
        $autoSaveInIcon = $this->iconFactory->getIcon('spinner-circle-dark', Icon::SIZE_SMALL);
        $autoSaveInLabel = $this->getLanguageService()->sL('LLL:EXT:autosave/Resources/Private/Language/locallang.xlf:button_autosave_in');
        $formNotReadyToSave = $this->getLanguageService()->sL('LLL:EXT:autosave/Resources/Private/Language/locallang.xlf:form_not_ready_to_save');

        $labels = [
            'autosave.buttonLoading' => $loadingButtonContent,
            'autosave.buttonAutosaveIn' => $autoSaveInIcon . ' ' . $autoSaveInLabel,
            'autosave.formNotReadyToSave' => $formNotReadyToSave
        ];

        $javaScriptCode = [];
        $javaScriptCode[] = 'if (typeof TYPO3 === \'undefined\' || typeof TYPO3.lang === \'undefined\') {';
        $javaScriptCode[] = '   TYPO3.lang = {}';
        $javaScriptCode[] = '}';
        $javaScriptCode[] = 'var additionalInlineLanguageLabels = ' . json_encode($labels) . ';';
        $javaScriptCode[] = 'for (var attributeName in additionalInlineLanguageLabels) {';
        $javaScriptCode[] = '   if (typeof TYPO3.lang[attributeName] === \'undefined\') {';
        $javaScriptCode[] = '       TYPO3.lang[attributeName] = additionalInlineLanguageLabels[attributeName]';
        $javaScriptCode[] = '   }';
        $javaScriptCode[] = '}';
        $this->pageRenderer->addJsInlineCode('autosave_localization', implode("\n", $javaScriptCode));
    }

    /**
     * Adds the settings of extension configuration (TYPO3.settings.Autosave)
     */
    protected function addJavaScriptSettings(): void
    {
        $interval = null;
        $serializedConfiguration = $GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['autosave'] ?? null;
        if ($serializedConfiguration !== null) {
            $configuration = unserialize($serializedConfiguration, ['allowed_classes' => false]);
            $interval = (int)$configuration['autosaveInterval'];
        }
        if ($interval === null || (int)$interval === 0) {
            $interval = self::DEFAULT_INTERVAL;
        }
        $this->pageRenderer->addInlineSetting('Autosave', 'interval', $interval);
    }

    /**
     * Returns the language service
     *
     * @return LanguageService
     */
    protected function getLanguageService(): LanguageService
    {
        return $GLOBALS['LANG'];
    }
}
