/*
 * This file is part of the "autosave" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */
define(['jquery'], function () {
	'use strict';

	/**
	 * Configuration
	 */
	let Autosave = {

		// Selectors
		_buttonSelector: '.btn-autosave',

		// Save duration (default value 300s, will be overwritten by TYPO3.settings.Autosave.interval
		_saveDuration: 300 * 1000,

		// Internal settings
		_saveTimeout: null,
		_saveStartTime: null,

		// Update the button every second to show the remaining time to the next autosave
		_infoDuration: 1000,
		_infoInterval: null,

		_showNotSavedNoticeDuration: 3000
	};

	/**
	 * Initialize the auto save
	 */
	Autosave.initialize = function () {
		// Set the interval by configuration
		Autosave._saveDuration = TYPO3.settings.Autosave.interval * 1000;

		// Switch autosave-status by a click on the button
		$(Autosave._buttonSelector).click(function (ev) {
			ev.preventDefault();
			Autosave.switchStatus();
			return false;
		});

		// Disable the button while processing
		Autosave.waitButton();

		// Load the autosave-status by AjaxController and enable the interval, if autosave is active
		$.ajax({
			url: TYPO3.settings.ajaxUrls.autosave_get_button_status,
			method: 'GET',
			dataType: 'json',

			// On success update the button and switch the interval-status
			success: function (response) {
				Autosave.setIntervalStatus(response);
			},

			// On error write the error message from TYPO3 to the page
			error: function (jqXHR, textStatus, errorThrown) {
				$('body').html(jqXHR.responseText);
			}
		});
	};

	/**
	 * Switch the auto save status (on => off, off => on)
	 */
	Autosave.switchStatus = function () {

		// Disable the button while processing
		Autosave.waitButton();

		// Disable the button while processing
		$.ajax({
			url: TYPO3.settings.ajaxUrls.autosave_switch_status,
			method: 'GET',
			dataType: 'json',
			success: function (response) {
				Autosave.setIntervalStatus(response);
			},
			error: function (jqXHR, textStatus, errorThrown) {
				$('body').html(jqXHR.responseText);
			}
		});
	};

	/**
	 * Switches the interval status
	 */
	Autosave.setIntervalStatus = function (response) {
		$(Autosave._buttonSelector).each(function () {
			$(this)
				.html(response.buttonContent)
				.attr('title', response.buttonTitle)
				.tooltip('fixTitle')
				.removeAttr('disabled');
		});
		if (response.status === true) {
			Autosave.startAutosave();
		} else {
			Autosave.stopAutosave();
		}
	};

	/**
	 * Starts the interval
	 */
	Autosave.startAutosave = function () {
		// Get the starttime to calculate the remaining time
		Autosave._saveStartTime = parseInt(new Date().getTime() / 1000);

		// Start the autosave-timeout
		Autosave._saveTimeout = setTimeout(function () {
			Autosave.autosave();
		}, Autosave._saveDuration);

		// Start the info interval (to show the current status on the button)
		Autosave._infoInterval = setInterval(function () {
			Autosave.updateIntervalInfo();
		}, Autosave._infoDuration);
	};

	/**
	 * Stops the interval
	 */
	Autosave.stopAutosave = function () {
		// Clear the autosave-timeout
		if (Autosave._saveTimeout !== null) {
			clearTimeout(Autosave._saveTimeout);
		}

		// Clear the info interval
		if (Autosave._infoInterval !== null) {
			clearInterval(Autosave._infoInterval);
		}
	};

	/**
	 * Updates the button content to show the remaining time until the next autosave
	 */
	Autosave.updateIntervalInfo = function () {
		if (Autosave._saveTimeout !== null) {
			let button = $(Autosave._buttonSelector);
			let now = parseInt(new Date().getTime() / 1000);
			let timeRemaining = parseInt(Autosave._saveDuration / 1000) - (now - Autosave._saveStartTime);
			if (timeRemaining >= 0) {
				if (!button.hasClass('btn-update-info')) {
					let autosaveIn = TYPO3.lang['autosave.buttonAutosaveIn'].replace('%s', timeRemaining);
					button.addClass('btn-update-info').html(autosaveIn);
				} else {
					$(Autosave._buttonSelector + ' .seconds').html(timeRemaining);
				}
			}
		}
	};

	/**
	 * Sets the button on "wait"-mode (loading..., disabled etc.)
	 */
	Autosave.waitButton = function () {
		$(Autosave._buttonSelector).each(function () {
			let waitContent = TYPO3.lang['autosave.buttonLoading'];
			$(this)
				.attr('disabled', 'disabled')
				.html(waitContent)
				.removeClass('btn-update-info');
		});
	};

	/**
	 * Sets all formfields to "changed" and submits the form
	 *
	 * The formfields must be set to changed, otherwise we lose the changes of the current field were the user is working on.
	 */
	Autosave.autosave = function () {
		// Set all fields to "changed"
		$('form[name="' + TBE_EDITOR.formname + '"] [name^="data["]').each(function () {
			let field = $(this);
			if (field.length > 0) {
				let name = field.attr('name');
				if (name !== undefined && field.closest('.t3js-formengine-palette-field').hasClass('has-change') === false && field.attr('type') !== 'hidden') {
					TBE_EDITOR.fieldChanged_fName(name, name);
				}
			}
		});

		// Check if the form is valid and submit
		if (TBE_EDITOR.checkSubmit(false) === true) {
			TBE_EDITOR.submitForm();
		} else {
			// If the form is not valid, show an information and start the interval again.
			let button = $(Autosave._buttonSelector);
			let currentTitle = button.attr('title');
			button
				.attr('title', TYPO3.lang['autosave.formNotReadyToSave'])
				.tooltip('fixTitle')
				.tooltip('show');
			setTimeout(function () {
				button
					.tooltip('hide')
					.attr('title', currentTitle)
					.tooltip('fixTitle');
			}, Autosave._showNotSavedNoticeDuration);
			Autosave.startAutosave();
		}
	};

	/**
	 * Initialize
	 */
	if ($(Autosave._buttonSelector).length > 0) {
		Autosave.initialize();
	}

	return Autosave;
});
