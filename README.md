# TYPO3 Extension `Autosave`

[![Donate](https://img.shields.io/badge/Donate-PayPal-green.svg?style=for-the-badge)](https://paypal.me/pmlavitto)
[![Latest Stable Version](https://img.shields.io/packagist/v/lavitto/typo3-autosave?style=for-the-badge)](https://packagist.org/packages/lavitto/typo3-autosave)
[![TYPO3](https://img.shields.io/badge/TYPO3-autosave-%23f49700?style=for-the-badge)](https://extensions.typo3.org/extension/autosave/)
[![License](https://img.shields.io/packagist/l/lavitto/typo3-autosave?style=for-the-badge)](https://packagist.org/packages/lavitto/typo3-autosave)

> This extension provides an automatic save function for TYPO3 backend.

- **Gitlab Repository**: [gitlab.com/lavitto/typo3-autosave](https://gitlab.com/lavitto/typo3-autosave)
- **TYPO3 Extension Repository**: [extensions.typo3.org/extension/autosave](https://extensions.typo3.org/extension/autosave/)
- **Found an issue?**: [gitlab.com/lavitto/typo3-autosave/issues](https://gitlab.com/lavitto/typo3-autosave/issues)

## 1. Introduction

### Features

- Simple and fast installation
- Automatic save for pages, contents etc.
- No configuration needed

### Screenshots

#### Autosave active

![Autosave active](https://cdn.lavitto.ch/typo3/lavitto/typo3-autosave/autosave-enabled_tmb.png)
- [Full Size Screenshot](https://cdn.lavitto.ch/typo3/lavitto/typo3-autosave/autosave-enabled.png)

#### Autosave inactive

![Autosave active](https://cdn.lavitto.ch/typo3/lavitto/typo3-autosave/autosave-disabled_tmb.png)
- [Full Size Screenshot](https://cdn.lavitto.ch/typo3/lavitto/typo3-autosave/autosave-disabled.png)

## 2. Installation

### Installation using Composer

The recommended way to install the extension is by using [Composer](https://getcomposer.org/). In your Composer based 
TYPO3 project root, just do `composer req lavitto/typo3-autosave`.

### Installation from TYPO3 Extension Repository (TER)

Download and install the extension `autosave` with the extension manager module.

## 3. Administration

### Enable / disable autosave function

The autosave function will be added in all edit forms were a "save"-button is available. It is included in the top right corner of the edit form (see screenshots).

**Simple click on the "Autosave"-button to enable or disable autosave.**

## 4. Configuration

This default properties can be changed by the **Global Extension Configuration**:

| Property           | Description                                                                        | Type      | Default value   |
| ------------------ | ---------------------------------------------------------------------------------- | --------- | --------------- |
| autosaveInterval   | Defines the interval (in seconds) were the contents will be saved automatically.   | int+      | 300             |

Change this properties by Admin Tools > Settings > Extension Configuration > Configure extensions > autosave.

## 6. Contribute

Please create an issue at https://gitlab.com/lavitto/typo3-autosave/issues.

**Please use GitLab only for bug-reporting or feature-requests. For support use the TYPO3 community channels or contact us by email.**

## 7. Support

If you need private or personal support, contact us by email on [info@lavitto.ch](mailto:info@lavitto.ch). 

**Be aware that this support might not be free!**
