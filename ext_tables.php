<?php
/**
 * This file is part of the "autosave" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

defined('TYPO3_MODE') || die();

/**
 * Add autosave icons
 */
$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
$iconRegistry->registerIcon(
    'ext-autosave-autosave',
    \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
    ['source' => 'EXT:autosave/Resources/Public/Icons/Backend/autosave.svg']
);
$iconRegistry->registerIcon(
    'ext-autosave-autosave-enabled',
    \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
    ['source' => 'EXT:autosave/Resources/Public/Icons/Backend/autosave_enabled.svg']
);
$iconRegistry->registerIcon(
    'ext-autosave-autosave-disabled',
    \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
    ['source' => 'EXT:autosave/Resources/Public/Icons/Backend/autosave_disabled.svg']
);

/**
 * Add the buttons hook
 */
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['Backend\Template\Components\ButtonBar']['getButtonsHook'][] = 'Lavitto\Autosave\Hooks\AutosaveHook->addAutosaveButton';
